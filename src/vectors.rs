pub fn run() {
    println!("-------------------arrays file-------------------");
    let mut numbers: Vec<i32> = vec![1, 2, 3, 4];

    //Re-assign value
    numbers[2] = 20;
    //Add on to vector
    numbers.push(5);
    numbers.push(6);
    numbers.pop();
    println!("{:?}", numbers);
    //get single val
    println!("single Value: {}", numbers[0]);
    //Get array length
    println!("Vector Length: {}", numbers.len());
    //Arrays are stack allocated
    println!("Vector occupies {} bytes", std::mem::size_of_val(&numbers));
    //Get Slice
    let slice: &[i32] = &numbers[0..2];
    println!("Slice: {:?}", slice);
    //Loop through vector values
    for x in numbers.iter() {
        println!("Number: {}", x);
    }

    //Loop and mutate vector values
    for x in numbers.iter_mut() {
        *x *= 2;
    }
    println!("Numbers in Vector: {:?}", numbers);
}