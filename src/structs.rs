//Structs - Used to create custom data types

//Traditional Struct
struct RGB {
    red: u8,
    green: u8,
    blue: u8,
}

//Tuple Struct
struct SRGB(u8, u8, u8, u8);

struct Person {
    first_name: String,
    last_name: String,
}

impl Person {
    //Construct person
    fn new(first: &str, last: &str) -> Person {
        Person {
            first_name: first.to_string(),
            last_name: last.to_string(),
        }
    }

    //Get full name
    fn full_name(&self) -> String {
        format!("{} {}", self.first_name, self.last_name)
    }

    //Set last name
    fn set_last_name(&mut self, last: &str) {
        self.last_name = last.to_string();
    }

    //Name to tuple
    fn to_tuple(self) -> (String, String) {
        (self.first_name, self.last_name)
    }
}

pub fn run() {
    println!("-------------------structs file-------------------");
    let mut c = RGB {
        red: 255,
        green: 0,
        blue: 0,
    };

    c.red = 200;

    println!("Color: {} {} {}", c.red, c.green, c.blue);

    let mut d = SRGB(255, 255, 0, 0);
    d.0 = 200;
    println!("Color: {} {} {} {}", d.0, d.1, d.2, d.3);

    let mut p = Person::new("Sarah", "Majin");
    println!("Person {}", p.full_name());
    p.set_last_name("Williams");
    println!("Person {}", p.full_name());
    println!("Person {:?}", p.to_tuple());
}