//Loops - Used to iterate until a condition is met

fn fizzbuzz(x: i8) {
    if x % 15 == 0 {
        println!("fizzbuzz");
    } else if x % 3 == 0 {
        println!("fizz");
    } else if x % 5 == 0 {
        println!("buzz")
    } else {
        println!("{}", x);
    }
}

pub fn run() {
    println!("-------------------loops file-------------------");

    let mut count = 0;
    //Infinite Loop
    loop {
        count += 1;
        println!("Number: {}", count);

        if count == 5 {
            break;
        }
    }

    //While Loop (FizzBuzz)
    count = 1;
    while count <= 100 {
        fizzbuzz(count);
        count += 1;
    }

    //For Range
    for x in 1..100 {
        fizzbuzz(x);
    }
}