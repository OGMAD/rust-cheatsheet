//Variables hold primitive data or references to data
//Variables are immutable by default
//Rust is a block-scoped language

pub fn run() {
    println!("-------------------vars file-------------------");
    let name = "Sarah";
    let mut age = 18;
    println!("My name is {} and i was {}", name, age);
    age = 19;
    println!("My name is {} and now i am {}", name, age);

    //Define const
    const ID: i32 = 001;
    println!("ID: {}", ID);

    //Assign multiple vars
    let (my_name, my_job) = ("Sarah", "Coder");
    println!("I am {} and i am a {}", my_name, my_job);
}