pub fn run() {
    //Print to console
    println!("-------------------print file-------------------");
    //Basic Formatting
    println!("{} is {} years old", "Sarah", "18");
    //Positional Arguments
    println!("{0} is {1} years old and {0} likes to {2}", "Sarah", "18", "play");
    //Named Arguments
    println!("{name} likes to play {activity}", name = "Sarah", activity = "football");
    //Placeholder traits
    println!("Binary: {:b} Hex: {:x} Octal: {:o}", 10, 10, 10);
    //Placeholder for debug trait
    println!("{:?}", (12, true, "hello"));
    //Basic math
    println!("10 + 10 = {}", 10 + 10);
}