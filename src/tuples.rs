//Tuples group together values of different types
//Max 12 Elements

pub fn run() {
    println!("-------------------tuples file-------------------");
    let person: (&str, &str, i8) = ("Sarah", "Berlin", 100);
    println!("{} works in {} and serves up to {} customers per day!", person.0, person.1, person.2);
}